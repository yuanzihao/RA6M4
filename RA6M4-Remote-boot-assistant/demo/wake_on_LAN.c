/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date             Author      Notes
 * 
 */
/*
 * 程序清单：从udp 客户端改来的wake_on_LAN
 * 程序功能：广播发送 wake_on_LAN 数据
*/
#include <rtthread.h>
#include <sys/socket.h> /* 使用BSD socket，需要包含sockets.h头文件 */
#include <netdb.h>
#include <string.h>
#include <finsh.h>
#include "board.h"
char mac1[6] = SELF_COMPUTER_MAC_ADDRESS;
char send_data[6+6*16]; /* 发送用到的数据 */
void wake_on_LAN(const char *url, uint32_t count)
{
    int sock, port;
    struct hostent *host;
    struct sockaddr_in server_addr;
    /* 通过函数入口参数url获得host地址（如果是域名，会做域名解析） */
    host = (struct hostent *) gethostbyname(url);
    port = 9;
    /* 创建一个socket，类型是SOCK_DGRAM，UDP类型 */
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        rt_kprintf("Socket error\n");
        return;
    }
        for(int i = 0; i<6;i++)
        {
            send_data[i]= 0xFF;
        }
        for (int i = 0; i < 16; i++)
        {
                for (int j = 0; j < 6; j++)
                {
                        send_data[6 + 6 * i + j] = mac1[j];
                }
        }
    /* 初始化预连接的服务端地址 */
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr = *((struct in_addr *)host->h_addr);
    rt_memset(&(server_addr.sin_zero), 0, sizeof(server_addr.sin_zero));
    int flag=1;
    setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &flag, sizeof(flag));  //给socketfd开放广播权限
    /* 总计发送count次数据 */
    while (count)
    {
        /* 发送数据到服务远端 */
        sendto(sock, send_data, strlen(send_data), 0,
               (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
        /* 线程休眠一段时间 */
        rt_thread_delay(50);
        /* 计数值减一 */
        count --;
    }
    /* 关闭这个socket */
    closesocket(sock);
}

void CMD_wakeOnLAN(void)
{
    rt_kprintf("Wake on LAN!\n");
    wake_on_LAN("192.168.0.255",10);
}

MSH_CMD_EXPORT(CMD_wakeOnLAN , wake up your computer up);


