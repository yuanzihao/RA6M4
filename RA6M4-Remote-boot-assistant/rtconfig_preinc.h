
#ifndef RTCONFIG_PREINC_H__
#define RTCONFIG_PREINC_H__

/* Automatically generated file; DO NOT EDIT. */
/* RT-Thread pre-include file */

#define AUTH_MODE_KEY
#define AUTH_WITH_NOTLS
#define HAVE_CCONFIG_H
#define MQTT_COMM_ENABLED
#define RT_USING_NEWLIB
#define _POSIX_C_SOURCE 1
#define __RTTHREAD__

#endif /*RTCONFIG_PREINC_H__*/
