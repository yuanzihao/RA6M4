/*-----------------data config start  -------------------*/
#include "qcloud_iot_export.h"
#include "qcloud_iot_import.h"
#include "lite-utils.h"


#define TOTAL_PROPERTY_COUNT 2

static sDataPoint    sg_DataTemplate[TOTAL_PROPERTY_COUNT];

typedef struct _ProductDataDefine {
    TYPE_DEF_TEMPLATE_BOOL m_computer_status;
    TYPE_DEF_TEMPLATE_BOOL m_computer_switch;
} ProductDataDefine;

static   ProductDataDefine     sg_ProductData;

static void _init_data_template(void)
{
    sg_ProductData.m_computer_status = 0;
    sg_DataTemplate[0].data_property.data = &sg_ProductData.m_computer_status;
    sg_DataTemplate[0].data_property.key  = "computer_status";
    sg_DataTemplate[0].data_property.type = TYPE_TEMPLATE_BOOL;

    sg_ProductData.m_computer_switch = 0;
    sg_DataTemplate[1].data_property.data = &sg_ProductData.m_computer_switch;
    sg_DataTemplate[1].data_property.key  = "computer_switch";
    sg_DataTemplate[1].data_property.type = TYPE_TEMPLATE_BOOL;
};
